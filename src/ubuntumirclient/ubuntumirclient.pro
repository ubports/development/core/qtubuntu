TARGET = qpa-ubuntumirclient
TEMPLATE = lib

QT -= gui
QT += core-private egl_support-private fontdatabase_support-private eventdispatcher_support-private linuxaccessibility_support-private theme_support-private dbus

CONFIG += plugin no_keywords qpa/genericunixfontdatabase

# Workaround egl_support-private USE'ing libdl. This is normally defined by
# 'CONFIG+=qt_build_config', but doing that would completely overwrite our
# QMAKE_CXXFLAGS. Also, this is not portable, but this module won't be used
# outside of Ubuntu Touch anyway, so :shrug:.
QMAKE_LIBS_LIBDL =

DEFINES += EGL_NO_X11
# For compatibility
DEFINES += MESA_EGL_NO_X11_HEADERS
# CONFIG += c++11 # only enables C++0x
QMAKE_CXXFLAGS += -fvisibility=hidden -fvisibility-inlines-hidden -std=c++11 -Werror -Wall
QMAKE_LFLAGS += -std=c++11 -Wl,-no-undefined

CONFIG += link_pkgconfig
PKGCONFIG += egl xkbcommon liblomiri-content-hub

# Look, I normally do not advocate automagic dependency like this, but in this
# case I cannot make "CONFIG+=..." works with subdir qmake. A work around is to
# use "qmake -r CONFIG+=..." afterwards, but I think it's cleaner to do this then
# try to make the d/rules do "qmake -r".

system($$pkgConfigExecutable() --exists lomiri-url-dispatcher) {
    PKGCONFIG += lomiri-url-dispatcher
} else:system($$pkgConfigExecutable() --exists url-dispatcher-1) {
    PKGCONFIG += url-dispatcher-1
    DEFINES += QTUBUNTU_USE_LEGACY_URL_DISPATCHER
} else {
    error("Neither lomiri-url-dispatcher nor url-dispatcher-1 is available")
}

system($$pkgConfigExecutable() --exists mir1client) {
    PKGCONFIG += mir1client
} else:system($$pkgConfigExecutable() --exists mirclient) {
    PKGCONFIG += mirclient
} else {
    error("Neither mir1client nor mirclient is available")
}

SOURCES = \
    qmirclientbackingstore.cpp \
    qmirclientclipboard.cpp \
    qmirclientcursor.cpp \
    qmirclientdebugextension.cpp \
    qmirclientdesktopwindow.cpp \
    qmirclientglcontext.cpp \
    qmirclientinput.cpp \
    qmirclientintegration.cpp \
    qmirclientnativeinterface.cpp \
    qmirclientplatformservices.cpp \
    qmirclientplugin.cpp \
    qmirclientscreen.cpp \
    qmirclientscreenobserver.cpp \
    qmirclientwindow.cpp \
    qmirclientappstatecontroller.cpp

HEADERS = \
    qmirclientbackingstore.h \
    qmirclientclipboard.h \
    qmirclientcursor.h \
    qmirclientdebugextension.h \
    qmirclientdesktopwindow.h \
    qmirclientglcontext.h \
    qmirclientinput.h \
    qmirclientintegration.h \
    qmirclientnativeinterface.h \
    qmirclientorientationchangeevent_p.h \
    qmirclientplatformservices.h \
    qmirclientplugin.h \
    qmirclientscreenobserver.h \
    qmirclientscreen.h \
    qmirclientwindow.h \
    qmirclientlogging.h \
    qmirclientappstatecontroller.h \
    ../shared/ubuntutheme.h

OTHER_FILES += \
    ubuntumirclient.json

# Installation path
target.path +=  $$[QT_INSTALL_PLUGINS]/platforms

INSTALLS += target
