Source: qtubuntu-gles
Priority: optional
Section: libs
Maintainer: UBports Developers <developers@ubports.com>
Build-Depends: debhelper-compat (= 13),
               libatspi2.0-dev,
               libegl1-mesa-dev,
               libfontconfig1-dev,
               libfreetype6-dev,
               libgles2-mesa-dev,
               libglib2.0-dev,
               libinput-dev,
               liblomiri-content-hub-dev,
               liblomiri-url-dispatcher-dev | liburl-dispatcher1-dev,
               libmir1client-dev,
               libmtdev-dev,
               libudev-dev,
               libxkbcommon-dev,
               libxrender-dev,
               qtbase5-gles-dev,
               qtbase5-private-gles-dev,
               quilt,
Standards-Version: 3.9.6
Homepage: https://gitlab.com/ubports/development/core/qtubuntu
Vcs-Git: https://gitlab.com/ubports/development/core/qtubuntu.git
Vcs-Browser: https://gitlab.com/ubports/development/core/qtubuntu

Package: qtubuntu-android
Architecture: amd64 i386 arm64 armhf
Multi-Arch: same
Conflicts: qtubuntu-desktop,
Replaces: qtubuntu (<< 0.52),
          qtubuntu-desktop,
Breaks: ubuntu-touch-session (<< 0.107),
        unity8 (<< 7.85),
Provides: qtubuntu,
Depends: ${misc:Depends},
         ${shlibs:Depends},
# ${shlibs:Depends} gives us "libqt5gui5 | libqt5gui5-gles", which isn't enough.
         libqt5gui5-gles,
Description: Qt plugins for Ubuntu Platform API (mobile) - OpenGLES
 QtUbuntu is a set of Qt5 components for the Ubuntu Platform API. It contains a
 QPA (Qt Platform Abstraction) plugin based on the Ubuntu Platform API and a
 legacy QPA plugin based on the compatibility layers. It also provides Qt
 bindings for Ubuntu Platform API features that are not exposed through the QPA
 plugins.
 .
 This variant of the package is for Android-based phones and tablets (built
 against the OpenGLES variant of qtbase).
